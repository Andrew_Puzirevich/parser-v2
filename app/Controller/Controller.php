<?php

namespace app\Controller\Controller;

use app\Service\ParseService\ParseService;
use app\Service\ErrorHandlingService\ErrorHandlingService;
use app\Model\HelpMessage\HelpMessage;
use app\Service\ReportService\ReportService;
use app\Model\ParseToCSV\ParseToCSV;

/**
 * Class Controller
 * @package app\Controller\Controller
 */
class Controller
{
    /**
     * @var ParseService
     */
    private $doParse;
    /**
     * @var HelpMessage
     */
    private $helpMessage;
    /**
     * @var ErrorHandlingService
     */
    private $errorHandling;
    /**
     * @var $err
     */
    private $err;
    /**
     * @var $reportService
     */
    private $reportService;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->doParse = new ParseService();
        $this->helpMessage = new HelpMessage();
        $this->errorHandling = new ErrorHandlingService($this->err);
        $this->reportService = new ReportService();
    }

    /**
     * method for parsing page
     */
    public function parse()
    {
        $this->doParse->parsePages();
    }

    /**
     * method for get list of valid commands
     */
    public function help()
    {
        $this->helpMessage->getHelpCommands();
    }


    /**
     * @param $errCode
     */
    public function errorHandle($errCode){
        $this->errorHandling->ErrorHandlingMess($errCode);
    }

    /**
     *
     */
    public function report(){
        echo 'Enter domain:'.PHP_EOL;
        $domain = readline('Enter domain:');
        $this->reportService->setTypeReport(new ParseToCSV());
        $this->reportService->openReport($domain);
    }
}