<?php

namespace app\Service\ErrorHandlingService;

/**
 * Class ErrorHandlingService
 * @package app\Service\ErrorHandlingService
 */
class ErrorHandlingService
{
    /**
     * @var $errCode
     */
    protected $errCode;
    /**
     * @var $message
     */
    public $message;

    /**
     * ErrorHandlingService constructor.
     * @param $errCode
     */
    public function __construct($errCode)
    {
        $this->errCode = $errCode;
    }

    /**
     * @param $errCode
     * @return string
     * Set error messages by error code
     */
    public function ErrorHandlingMess($errCode)
    {
        $message = $this->message;

        switch ($errCode){
            case 'code 1':
                echo 'URl does not exist or isn`t valid!'.PHP_EOL.'Enter new command'.PHP_EOL;
                break;
            case 'code 0':
                echo 'Invalid command'.PHP_EOL;
                break;

            default;
        }
        return $message;
    }
}