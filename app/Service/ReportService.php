<?php

namespace app\Service\ReportService;

use app\Model\ParseInterface\ParseInterface;

/**
 * Class ReportService
 * @package app\Service\ReportService
 */
class ReportService
{
    /**
     * @var
     */
    private  $actionParseData;


    /**
     * @param ParseInterface $actionParseData
     */
    public function setTypeReport(ParseInterface $actionParseData)
    {
        $this->actionParseData = $actionParseData;
    }

    /**
     * @param $domain
     */
    public function openReport($domain)
    {
        $this->actionParseData->setDomain($domain);
        $this->actionParseData->load();
        echo PHP_EOL;
    }
}