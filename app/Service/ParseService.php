<?php

namespace app\Service\ParseService;

use app\Model\Url\Url;
use app\Model\Parser\Parser;
use app\Model\ParseToCSV\ParseToCSV;
use app\Model\Operations\Operations;

/**
 * Class ParseSite
 * @package app\Service\ParseSite
 */
class ParseService
{
    /**
     * @var $fileSavedMame
     */
    public $fileSavedMame;
    /**
     * Here we check the URL
     */
    public function parsePages()
    {
        echo 'Enter your URL'.PHP_EOL;

        $mainUrl = readline('Enter URL');
        $url = new Url($mainUrl);
        $url = $url->checkUrl($url);

        if ($url != 'error'){
            $saveDomain = $this->parseAllElemByURL($url);
            if ($saveDomain){
                $this->findUrlOnPage($url, $saveDomain);
                echo 'Parse finish, enter new command:'.PHP_EOL;
            }

        }
        else {
            echo 'URl does not exist or isn`t valid!'.PHP_EOL.'Enter new command'.PHP_EOL;
        }
    }


    /**
     * Here we will pars the pages for the specified URL,
     * & saving the CSV report
     * @param $url
     * @return bool|string
     */
    public function parseAllElemByURL($url)
    {
        $dom = new \DOMDocument();
        $parser = new Parser($url, $dom);
        $page = $parser->getPage();
        $parser->getPageObject($page);
        $images = $parser->getTags('img');
        $data = new Operations();
        $saveIn = new ParseToCSV();
        $saveIn->setUrl($url);
        $saveIn->setDomain($url);
        $saveIn->setData($images);
        $data->setSourceType($saveIn);
        $res = $data->save();

        if ($res) {
            $src = $saveIn->getFullSrc();
            echo 'File saved, new url added'.PHP_EOL.'Located - '.$src.PHP_EOL;
            return $this->fileSavedMame = $saveIn->getDomain($url);
        }
        else{
            echo 'Can`t save file'.PHP_EOL;
            return 'error';
        }
    }

    /**
     * Method find all URLs on set page & do recursive save img from new page on current domain
     * @param $url
     */
    public function findUrlOnPage($url, $saved)
    {
        $url = new Url($url);
        $arr = $url->getUrlFromPage($url);

        foreach ($arr as $item) {
            $domain = new ParseToCSV();
            $domainFull = $domain->getDomain($item);
            $saved = $domain->getFileName($saved);
            $domainClear = $domain->getFileName($domainFull);
            if ($saved == $domainClear){
                $this->parseAllElemByURL($item);
            }
        }

    }
}