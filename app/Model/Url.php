<?php

namespace app\Model\Url;

/**
 * Class Url
 * @package App\Model\Url
 */
class Url
{

    /**
     * @var string $url
     */
    protected $url;

    /**
     * Url constructor.
     * @param string $url
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * @param string $url
     * @return string
     * check url & return it, if exist
     */
    public function checkUrl($url)
    {
        $arrUrl = parse_url($url->url);

        if(empty($arrUrl['host']) && empty($arrUrl['path'])) {
            return 'error';
        }

        if (empty($arrUrl['scheme']) || !in_array($arrUrl['scheme'], array('http', 'https'))) {
            $arrUrl['scheme'] = 'http';
            $url->url = "{$arrUrl["scheme"]}://{$arrUrl["path"]}";
        }

        if ($this->isUrlExist($url->url)){
            return $url->url;
        }
        else{
            return 'error';
        }
    }

    public function getUrlFromPage($url)
    {
        $html = file_get_contents($url->url);
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $xpath = new \DOMXPath($dom);
        $hrefs = $xpath->evaluate("/html/body//a");
        $arr = array();

        for ($i = 0; $i < $hrefs->length; $i++) {
            $href = $hrefs->item($i);
            $url = $href->getAttribute('href');

            array_push($arr, $url);
        }
        return $arr;
    }

    /**
     * @param $url
     * @return bool
     * URL availability check
     */
    private function isUrlExist($url)
    {
        $headers = get_headers($url);

        if ($headers[0] == 'HTTP/1.0 200 OK' || $headers[0] == 'HTTP/1.1 200 OK'){
            return true;
        }
        else {
            return false;
        }

    }
}