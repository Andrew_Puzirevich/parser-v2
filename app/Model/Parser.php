<?php

namespace app\Model\Parser;

class Parser
{
    private $url;
    private $dom;

    public function __construct($url, \DOMDocument $dom)
    {
        $this->url = $url;
        $this->dom = $dom;
    }

    public function getPage(){
        $page = file_get_contents($this->url);
        return $page;
    }

    public function getPageObject($page){
        $dom = $this->dom;
        $dom->recover = true;
        $dom->strictErrorChecking = false;
        $dom->loadHTML($page);
        return $dom;
    }

    public function getTags($tag){
        $dom = $this->dom;
        $elements = $dom->getElementsByTagName($tag);
        return $elements;
    }
}