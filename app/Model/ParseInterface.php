<?php

namespace app\Model\ParseInterface;

/**
 * Interface ParseInterface
 * @package app\Model\ParseInterface
 */
interface ParseInterface
{
    public function save();
    public function load();
    public function setDomain($domain);
}