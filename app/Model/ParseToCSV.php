<?php

namespace app\Model\ParseToCSV;

use app\Model\ParseInterface\ParseInterface;

/**
 * Class ParseToCSV
 * @package app\Model\ParseToCSV
 */
class ParseToCSV implements ParseInterface
{
    /**
     * @var $data
     */
    private $data;
    /**
     * @var $url
     */
    private $url;
    /**
     * @var $domain
     */
    private $domain;

    /**
     * @param $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @param $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @param $url
     * @return bool
     */
    public function getDomain($url)
    {
        $arr = parse_url($url);
        $domain = isset($arr['host']) ? $arr['host'] : $arr['path'];
        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
            return $regs['domain'];
        }
        return false;
    }


    /**
     * @param null $domain
     * @return mixed
     */
    public function getFileName($domain = null)
    {
        if ($domain == null){
            $domain = $this->getDomain($this->domain);
        }

        $arr = explode(".", $domain, 2);
        $fileName = $arr[0];
        return $fileName;
    }

    /**
     * @param $domain
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    }

    /**
     * @return string
     */
    private function getFolder(){
        $cwd = getcwd();
        $folder = $cwd . '\\reports\\';
        return $folder;
    }

    /**
     * @return string
     */
    public function getFullSrc()
    {
        $folder = $this->getFolder();
        $fileName = $this->getFileName();
        $str = $folder . $fileName . '.csv';

        return $str;
    }

    /**
     * @return bool
     */
    public function save()
    {
        $fp = fopen($this->getFullSrc(), 'a+');
        foreach ($this->data as $img){
            $url='url:' . $img->getAttribute('src');
            $val = explode("," , $url);
            fputcsv($fp, $val);
        }
        return  fclose($fp);
    }


    /**
     * @return array
     */
    private function getFilesList()
    {
        $folder = $this->getFolder();
        $list = scandir($folder);
        return $list;
    }

    /**
     * @return bool
     */
    private function getReportExist()
    {
        if (!empty($this->getFilesList()))
        {
            foreach ($this->getFilesList() as $fileName)
            {
                if ($this->getFileName().'.csv' == $fileName )
                {
                    return true;
                }
            }
            return false;
        }
        return false;
    }


    /**
     * @return bool
     */
    public function load()
    {
        if ($this->getReportExist()){
            $src  = $this->getFolder().$this->getFileName().'.csv';
            $f = fopen($src, "r") or die("Unable to open file!");
            echo fread($f,filesize($src));
            return fclose($f);

        }

        return false;

    }
}