<?php

namespace app\Model\Operations;

use app\Model\ParseInterface\ParseInterface;

/**
 * Class Operations
 * @package app\Model\Operations
 */
class Operations
{
    /**
     * @var
     */
    private $source;

    /**
     * @param ParseInterface $source
     */
    public function setSourceType(ParseInterface $source)
    {
        $this->source = $source;

    }

    /**
     * @return mixed
     */
    public function save()
    {
        return  $this->source->save();
    }

    /**
     * @return mixed
     */
    public function load()
    {
        return $this->source->load();
    }

}