<?php
error_reporting(1);
require __DIR__."/vendor/autoload.php";

use app\Controller\Controller\Controller;

$controller = new Controller();

echo "Parser for sites. Ver 0.1".PHP_EOL."Enter 'help', to display a list of valid commands".PHP_EOL;

do{
    $instruction = readline('Enter the required command:');
    switch ($instruction){
        case 'parse':
            $controller->parse();
            break;
        case 'help':
            $controller->help();
            break;
        case 'report':
            $controller->report();
            break;
        case 'exit':
            exit('See you later, bye!');
        default :
            $error = 'code 0';
            $controller->errorHandle($error);
            break;
    }
}
while ($instruction != 'exit');